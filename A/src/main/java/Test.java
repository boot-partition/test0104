import com.yh.javabean.Student;
import com.yh.util.MyJDBCutil;

import java.util.ArrayList;

public class Test {
    public static void main(String[] args) {
        String sql="select * from student";
        ArrayList<Student> dql = MyJDBCutil.dql(sql, Student.class);
        for (Student student : dql) {
            System.out.println(student);
        }
    }
}
