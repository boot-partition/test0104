import com.yh.javabean.Teacher;
import com.yh.util.MyJDBCutil;

import java.util.ArrayList;

public class Test {
    public static void main(String[] args) {
        String sql="select * from teacher";
        ArrayList<Teacher> dql = MyJDBCutil.dql(sql, Teacher.class);
        for (Teacher teacher : dql) {
            System.out.println(teacher);
        }
    }
}
