package com.yh.javabean;

public class Teacher {
	private Integer tid;
	private String username;
	private String password;
	private String tname;
	private String tdesc;

	public Teacher() {
		super();
	}

	public Teacher(Integer tid,String username,String password,String tname,String tdesc) {
		super();
		this.tid = tid;
		this.username = username;
		this.password = password;
		this.tname = tname;
		this.tdesc = tdesc;
	}

	public Integer getTid() {
		return this.tid;
	}

	public void setTid(Integer tid) {
		this.tid = tid;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getTname() {
		return this.tname;
	}

	public void setTname(String tname) {
		this.tname = tname;
	}

	public String getTdesc() {
		return this.tdesc;
	}

	public void setTdesc(String tdesc) {
		this.tdesc = tdesc;
	}

	@Override
	public String toString() {
		return "Teacher [tid= "+ tid + ", username= "+ username + ", password= "+ password + ", tname= "+ tname + ", tdesc= "+ tdesc + "]";
	}
}