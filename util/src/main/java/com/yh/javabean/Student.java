package com.yh.javabean;

public class Student {
	private Integer sid;
	private String username;
	private String password;
	private String name;
	private Integer tid;
	private Double score;
	private Integer status;

	public Student() {
		super();
	}

	public Student(Integer sid,String username,String password,String name,Integer tid,Double score,Integer status) {
		super();
		this.sid = sid;
		this.username = username;
		this.password = password;
		this.name = name;
		this.tid = tid;
		this.score = score;
		this.status = status;
	}

	public Integer getSid() {
		return this.sid;
	}

	public void setSid(Integer sid) {
		this.sid = sid;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getTid() {
		return this.tid;
	}

	public void setTid(Integer tid) {
		this.tid = tid;
	}

	public Double getScore() {
		return this.score;
	}

	public void setScore(Double score) {
		this.score = score;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Student [sid= "+ sid + ", username= "+ username + ", password= "+ password + ", name= "+ name + ", tid= "+ tid + ", score= "+ score + ", status= "+ status + "]";
	}
}