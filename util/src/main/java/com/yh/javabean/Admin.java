package com.yh.javabean;

public class Admin {
	private Integer id;
	private String username;
	private String password;
	private String desc;

	public Admin() {
		super();
	}

	public Admin(Integer id,String username,String password,String desc) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.desc = desc;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDesc() {
		return this.desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	@Override
	public String toString() {
		return "Admin [id= "+ id + ", username= "+ username + ", password= "+ password + ", desc= "+ desc + "]";
	}
}